export default interface User {
  cell: string;
  dob: DateOfBirth;
  email: string;
  gender: string;
  id: ID;
  location: place;
  login: signIn;
  name: Name;
  nat: string;
  phone: string;
  picture: Photo;
  registered: register;
}

interface Photo {
  large: string;
  medium: string;
  thumbnail: string;
}

interface Name {
  title: string;
  first: string;
  last: string;
}

interface DateOfBirth {
  date: string;
  age: number;
}

interface ID {
  name: string | '';
  value: string | null;
}

interface place {
  city: string;
  coordinates: coordinate;
  country: string;
  postcode: string;
  state: string;
  street: streetProps;
  timezone: tz;
}

interface streetProps {
  number: number;
  name: string;
}

interface tz {
  offset: string;
  description: string;
}

interface coordinate {
  latitude: string;
  longitude: string;
}

interface signIn {
  md5: string;
  password: string;
  salt: string;
  sha1: string;
  sha256: string;
  username: string;
  uuid: string;
}

interface register {
  age: number;
  date: string;
}
