import { Component, OnInit } from '@angular/core';
import User from 'src/models/User';
import UserService from 'src/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users: User[] = [];

  constructor(private usersService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.usersService.getAllUser().then((data) => {
      this.users = data;
      localStorage.setItem('myArray', JSON.stringify(this.users));
    });
  }

  gotoUser(email: string) {
    this.router.navigate([`/users/view/${email}`]);
  }
}
