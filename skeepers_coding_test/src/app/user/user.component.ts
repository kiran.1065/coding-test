import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import User from 'src/models/User';
import UserService from 'src/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  user?: User;
  editMode: boolean = false;
  value = 'Edit User';
  constructor(
    private router: ActivatedRoute,
    private usersService: UserService
  ) {}

  ngOnInit(): void {
    const email = this.router.snapshot.paramMap.get('email');
    const action = this.router.snapshot.paramMap.get('action');
    console.log(action);
    if (action === 'edit') {
      this.editMode = true;
    } else {
      this.editMode = false;
    }
    console.log({
      USERS: JSON.parse(localStorage.getItem('myArray') as string),
    });
    const users: User[] = JSON.parse(localStorage.getItem('myArray') as string);
    this.user = users.find((user) => user.email === email);
    console.log({ user: this.user });
    console.log('here');
  }

  editDetails() {
    this.editMode = true;
  }
}
