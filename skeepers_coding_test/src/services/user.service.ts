import axios from 'axios';
import User from 'src/models/User';

export default class UserService {
  baseURL = 'https://randomuser.me/api/';

  async getAllUser(): Promise<User[]> {
    const res = await axios.get(this.baseURL + '?results=20');
    // console.log(res);
    // console.log(res.data.results);
    return res.data.results;
  }
}
